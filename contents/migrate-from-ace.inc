<p class="focus">Cisco recommends Citrix NetScaler as the preferred next-generation ADC solution to their customers worldwide</p>

<p>Citrix NetScaler and Cisco ACI enable datacenter and cloud administrators to holistically control L2-L7 network services in a unified manner via seamless insertion and automation of best-in-class NetScaler services into next-generation datacenters built on Cisco’s ACI Architectures.</p>
<ul>
	<li>Moving on from Cisco ACE to a software defined world: View On-Demand Webinar</li>
	<li>Citrix NetScaler Integration with Cisco Nexus Switch Product Line: Learn more</li>
	<li>NetScaler Solutions Brief: Cisco ACE, Nexus and ACI Integration: Download now</li>
</ul>

<h2>1-	Why migrate</h2>

<ul>
	<li>Capacity Exceeded: Existing CSS/CSM/ACE throughput, SSL offload not scaling Hyper Converse Infrastructure: Hyper-converged technology could benefit DCV environment and the infrastructure need to be ready Advanced</li>
	<li>Features Required: Availability, app performance, security, scalability, QoS</li>
	<li>Timing: Depreciated assets, lease termination, SmartNet end, EOS, EoL, EoS</li>
	<li>Data Center Projects: Data Center consolidation, technology refresh, new application rollouts, Server virtualization, Desktop virtualization</li>
	<li>ACI: Prepare Data Center Infrastructure for ACI/SDN business transformation, protect business assets</li>
</ul>

<h2>2- Proposal and Promotion until March 31st 2016</h2>

To assist our mutual customers migrate from existing Cisco ACE deployments to Citrix NetScaler, Citrix is offering an ACE Migration Program (AMP). This limited-time program provides generous discounts for NetScaler product purchases and Citrix ADC migration services.

<p class="focus">Qualified Cisco ACE customers will receive:</p>

<ul>
	<li><span class="text-primary">20% ADDITIONAL discount</span> on the list price of any NetScaler MPX and multi-tenant NetScaler SDX appliance, please use the promo code: <span class="text-primary">EMEAMPROM</span> to apply product discount.</li>
	<li><span class="text-primary">20% Additional Education promo</span>: Cisco ACE customers can register here to secure your 20% discount off the CNS-208 Citrix NetScaler 10.5 Essentials for ACE Migration training.</li>
</ul>

<p>Note: The above discounts are applied to Citrix list prices. Existing volume license discount levels will apply to the discounted list price, if applicable.
<br>To benefit from the discount, Consulting Services must be consumed in a contiguous block and initiated within 1 month of receipt of Purchase Order.</p>

<p class="focus">Eligible Cisco Products:</p>

<ul>
	<li>Cisco Application Control Engine Module or Appliance</li>
	<li>Cisco Content Services Switch (CSS)</li>
	<li>Cisco Content Switch Module (CSM)</li>
	<li>Cisco Global Site Selector (GSS)</li>
</ul>
<ul class="fa-ul">
	<li><i class="fa fa-li fa-file-pdf-o text-primary"></i> <a href="http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/cisco-ace-migration-projects.pdf" target="_blank">Selecting the Right NetScaler Solution for Cisco ACE Migrations</a></li>
	<li><i class="fa fa-li fa-file-pdf-o text-primary"></i> <a href="http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/citrix-netscaler-application-delivery-controller-at-a-glance.pdf" target="_blank">NetScaler At-A-Glance</a></li>
	<li><i class="fa fa-li fa-file-pdf-o text-primary"></i> <a href="http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/citrix-netscaler-deployment-guide.pdf" target="_blank">NetScaler Deployment Guide</a></li>
</ul>

<h2>3- Smooth transition: ACE Test Drive Workshop</h2>

<p>To support ACE customer on that business transformation, we develop ACE Test Drive workshops, those workshops include structured education and hands-on experience to show customers how a new technology works and meets business needs. After completing this workshop, attendees will be able to understand the technical and business benefits that the NetScaler ADC provides, as well as perform the installation and configuration tasks required for the initial deployment of a NetScaler ADC solution.</p>
<ul>
	<li>Cisco ACE Migration Planning</li>
	<li>Platform Setup</li>
	<li>Platform Configuration</li>
	<li>Configuration Migration</li>
	<li>Knowledge Transfer</li>
</ul>

<p>To enroll in the ACE test Drive workshop, please contact your Citrix Solution Advisor or Citrix representative.</p>

<p class="focus">Ace migration Tool: how to convert an ACE configuration file to a NetScaler configuration file</p>

<ul class="fa-ul">
	<li><i class="fa fa-li fa-youtube text-primary"></i> <a href="embed_youtube.php?v=NrwPUz9Nnvc" data-toggle="modal" data-target="#myModal" data-remote="false">How to use the migration tool (1:59)</a></li>
</ul>

<p class="text-center"><iframe class="video" width="640" height="360" src="https://www.youtube.com/embed/NrwPUz9Nnvc" frameborder="0" allowfullscreen></iframe></p>

<p class="text-center">
	<i class="fa fa-external-link text-primary"></i> <a href="http://cis.citrix.com/" target="_blank">Access the tool</a>
</p>

<h2>4- Move to Citrix NetScaler Today</h2>

<p>The ACE Migration Program is a limited-time offer. To qualify, Cisco ACE customers should contact a Citrix Solution Advisor to confirm eligibility and to begin an evaluation of Citrix NetScaler ADC solutions. Eligible customers must submit their qualifying purchase order for NetScaler no later than March 24, 2016, and either return the Cisco product being replaced by Citrix NetScaler or provide a corresponding Certificate Of Destruction within 90 days of order fulfillment.</p>

<p>For more details, please contact your <a href="https://www.citrix.com/buy/partnerlocator.html" target="_blank">Citrix Solution Advisor</a> or Citrix representative. 
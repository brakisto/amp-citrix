<?php
switch($_GET['URL']) {
	case 'nexus-7000':
		$ID_menu = 1;
		$title = 'NetScaler appears as a remote services blade on the Cisco Nexus<br/>7000 Series switch';
		$banner = 'nexus.jpg';
		$content = "<p>Leveraging Cisco RISE technology, NetScaler seamlessly integrates with the Cisco Nexus 7000 series switch for simplified deployment, automated control and lower cost of operations while delivering the standalone performance, availability and reliability of a best-in-class ADC. </p>";
		$links = array(
			array(
				'blog',
				'Cisco blog: NetScaler integration via Cisco RISE',
				'http://blogs.cisco.com/datacenter/rise'
			),
			array(
				'youtube',
				'Learn about Citrix NetScaler enabled by Cisco RISE (2:24)',
				'CIciKi9nQMo'
			),
			array(
				'pdf',
				'Citrix NetScaler and Cisco Nexus Integration Solution Brief',
				'http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/cisco-nexus-7000-series-integration.pdf'
			),
			array(
				'pdf',
				'Deliver the Next-Generation Intelligent Data Center with Cisco Nexus 7000 Series Switches, Citrix NetScaler Application Delivery Controller, and RISE Technology',
				'http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/deliver-next-generation-intelligent-data-center-with-cisco-nexus-7000-series-switches.pdf'
			),
		);
		break;

	case 'netscaler-1000v':
		$ID_menu = 2;
		$title = 'Expanding the virtual network infrastructure portfolio';
		$banner = '1000v.jpg';
		$content = "<p>Citrix and Cisco have delivered the Citrix NetScaler 1000V, a best-in-class application delivery controller that redefines L4-L7 services. Building on Cisco strength in Unified Fabric and Unified Compute, and Citrix strength in intelligent, application delivery, the architecture runs on the Nexus Virtual Service Platform to provide a <a href=\"http://www.citrix.com/products/netscaler-application-delivery-controller/overview.html\" target=\"_blank\">virtual network infrastructure</a> that delivers scalability, elastic instantiation, and multi-tenant operation, all with a common approach to service provisioning and management.</p>
<p>The Citrix NetScaler 1000V is available via the normal Cisco sales and worldwide channels, providing a smooth migration path for Cisco ACE, GSS and CSS customers. Citrix NetScaler is the Cisco ACE replacement product recommended by Cisco. 
</p>";
		$links = array(
			array(
				'press',
				'Press release: Cisco Integrates Citrix NetScaler into Cloud Network Services Portfolio',
				'http://www.citrix.com/news/announcements/jun-2013/cisco-integrates-citrix-netscaler-into-cloud-network-services-po.html'),
			array(
				'blog',
				'Blog: Citrix and Cisco: A New Playbook for New Times',
				'http://blogs.citrix.com/2013/06/20/citrix-and-cisco-a-new-playbook-for-new-times'
			),
		);
		break;

	case 'migrate-from-ace':
		$ID_menu = 3;
		$title = 'Making it simple to migrate from Cisco ACE to Citrix NetScaler ADC solution';
		$banner = 'migrate.jpg';
		$content = "";
		$links = array();
		break;

	default:
		$ID_menu = 0;
		$title = 'Citrix and Cisco deliver business and application agility with ACI';
		$banner = 'journey.jpg';
		$content = "<p>Citrix NetScaler and Cisco ACI enable datacenter and cloud administrators to holistically control L2-L7 network services in a unified manner via seamless insertion and automation of best-in-class NetScaler services into next-generation datacenters built on Cisco's ACI Architectures. NetScaler leverages the Cisco Application Policy Infrastructure Controller (APIC) to programmatically automate network provisioning and control based on application requirements and policies for both datacenter and enterprise environments.</p>";
		$links = array(
			array(
				'youtube',
				'Citrix is now ACI Ready',
				'WrweorXbqmo'
			),
			array(
				'video',
				'View webinar: Transform your datacenter with Cisco ACI and Citrix NetScaler',
				'http://www.citrix.com/events/transform-your-data-center-with-cisco-aci-and-citrix-netscaler.html'
			),
			array(
				'blog',
				'Citrix NetScaler and ACI Integration – the Real SDN',
				'http://blogs.citrix.com/?p=174203302'
			),
			array(
				'pdf',
				'Cisco and Citrix: Building Application Centric, ADC-enabled Data Centers',
				'http://www.citrix.com/content/dam/citrix/en_us/documents/products-solutions/cisco-and-citrix-adc-solution-brief.pdf'
			),
		);
		break;
}
?>
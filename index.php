<?php

ERROR_REPORTING(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
$IZiphone = strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') ? TRUE : FALSE;
$IZipad = strstr($_SERVER['HTTP_USER_AGENT'],'iPad') ? TRUE : FALSE;

include('contents/contents.inc');
include('header.inc');
echo '<body>'."\n";

if (!$IZipad) {
	echo '<div id="mobile-menu">'."\n";
	include('menu_mobile.inc');
	echo '</div>'."\n";

	echo '<div id="fullcontent">'."\n";
}

include('navbar.inc');

echo '	<div class="container">'."\n";
echo '		<div class="row">'."\n";
echo '			<div class="col-md-12">'."\n";
echo '				<div class="banner"><img src="img/'.$banner.'"></div>'."\n";
echo '				<div class="content">'."\n";
echo '					<h1>'.$title.'</h1>'."\n";
if (!$content) {
	readfile('contents/'.$_GET['URL'].'.inc');
} else {
	echo '					'.$content."\n";
}
echo '				</div>'."\n";
echo '			</div>'."\n";
echo '		</div>'."\n";

if (count($links)) {
	echo '		<div class="row">'."\n";
	echo '			<div class="col-md-12">'."\n";
	echo '				<ul class="fa-ul content">'."\n";
	$i = 0;
	foreach ($links as $link) {
		list($type, $libelle, $URL) = $link;
		switch($type) {
			case 'video': $icone = 'play'; break;
			case 'pdf': $icone = 'file-pdf-o'; break;
			case 'blog': $icone = 'rss'; break;
			case 'press': $icone = 'newspaper-o'; break;
			default: $icone = $type; break;
		}
		echo '<li><i class="fa fa-li fa-'.$icone.' text-primary"></i> '."\n";
		echo $type == 'youtube'
			? '<a href="embed_youtube.php?v='.$URL.'" class="only-desktop" data-toggle="modal" data-target="#myModal" data-remote="false">'.$libelle.'</a>'
				.'<p class="text-center only-mobile"><iframe class="video" width="640" height="360" src="https://www.youtube.com/embed/'.$URL.'" frameborder="0" allowfullscreen></iframe></p>'
			: '<a href="'.$URL.'" target="_blank">'.$libelle.'</a>'."\n";
		echo '</li>'."\n";
		$i++;
	}
	echo '				</ul>'."\n";
	echo '			</div>'."\n";
	echo '		</div>'."\n";

}

echo '	</div>'."\n";

echo '	<p>&nbsp;</p>'."\n";
echo '	<p>&nbsp;</p>'."\n";
echo '	<p>&nbsp;</p>'."\n";
echo '	<div class="container">'."\n";
echo '		<div class="row">'."\n";
echo '			<div class="col-md-12">'."\n";
echo '				<div class="content text-center" style="border-top: gainsboro solid 1px; padding: 20px 0;">'."\n";
echo '					<p class="copyright text-muted">&copy;2015 Citrix Systems, Inc. All rights reserved.  |  4988 Great America Parkway, Santa Clara CA 95054 USA  |  *All trademarks are the property of their respective owners.</p>'."\n";
echo '				</div>'."\n";
echo '			</div>'."\n";
echo '		</div>'."\n";
echo '	</div>'."\n";

echo '	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'."\n";
echo '	  <div class="modal-dialog" style="z-index: 1000;">'."\n";
echo '	    <div class="modal-content">'."\n";
echo '	      <div class="modal-body">'."\n";
echo '	        ...'."\n";
echo '	      </div>'."\n";
echo '	      <div class="modal-footer">'."\n";
echo '	        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Close</button>'."\n";
echo '	      </div>'."\n";
echo '	    </div>'."\n";
echo '	  </div>'."\n";
echo '	</div>'."\n";

if (!$IZipad) {
	echo '</div>'."\n";		// fullcontent
}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-39345190-6', 'auto');
  ga('send', 'pageview');
</script>
<?
echo '</body>'."\n";

include('footer.inc');
?>
	<nav class="navbar navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div id="sliding-switcher" class="drawer-toggle"><i class="fa fa-2x fa-bars text-muted"></i></div>
				<a class="navbar-brand" href="index.html" id="logo-index"></a>
				<div class="clear"></div>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li<? echo $ID_menu == 0 ? ' class="active"' : '';?>><a href="journey-to-aci-900.html">Journey to ACI/900</a></li>
					<li<? echo $ID_menu == 1 ? ' class="active"' : '';?>><a href="nexus-7000.html">Nexus 7000</a></li>
					<li<? echo $ID_menu == 2 ? ' class="active"' : '';?>><a href="netscaler-1000v.html">NetScaler 1000v</a></li>
					<li<? echo $ID_menu == 3 ? ' class="active"' : '';?>><a href="migrate-from-ace.html">Migrate from ACE</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

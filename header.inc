<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7, IE=9" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-title" content="<?=$title;?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0, minimal-ui">
		
		<title><?=$title;?></title>
		<meta name="description" content="<?=$description;?>" />
		<meta name="keywords" content="<?=$keywords;?>" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.png">
		<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="favicon.png">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap.bypass.css">
		<link rel="stylesheet" type="text/css" href="http://s3.amazonaws.com/citrix-cdn/can.cdn/marketing/assets/fonts/citrix-fonts.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/amp.css">

		<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/slideout.min.js"></script>
	</head>

	<script type="text/javascript">
	function leftPanel() {
		var lv = $('.video').width();
		var hv = lv / 1.777;
		$('.video').height(hv);
		var largeur = $( window ).width();
		if (largeur < 750) {
			var slideout = new Slideout({
			  'panel': document.getElementById('fullcontent'),
			  'menu': document.getElementById('mobile-menu'),
			  'padding': 250,
			  'tolerance': 70
			});
			document.querySelector('#sliding-switcher').addEventListener('click', function() {
				slideout.toggle();
			});
		}
	}
	$( document ).ready(function(){
		leftPanel();
		$("#myModal").on("show.bs.modal", function(e) {
			var link = $(e.relatedTarget);
			$(this).find(".modal-body").load(link.attr("href"));
		});
	});
	$(window).resize(leftPanel);
	</script>
</html>